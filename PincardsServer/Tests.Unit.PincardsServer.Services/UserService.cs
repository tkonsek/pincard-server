﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PincardsServer.Services;

namespace Tests.Unit.PincardsServer.Services
{
    [TestClass]
    public class Tests_Unit_UserService
    {
        [TestClass]
        public class Tests_Unit_UserService_RegisterUser
        {
            #region invalid email
            [TestMethod]
            [ExpectedException(typeof(InvalidArgumentException), "invalid_email")]
            public void ShouldThrowInvalidArgumentExceptionOnEmailNull()
            {
                IUserService s = new UserService();
                s.RegisterUser(null, "");
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidArgumentException), "invalid_email")]
            public void ShouldThrowInvalidArgumentExceptionOnEmailEmpty()
            {
                IUserService s = new UserService();
                s.RegisterUser("", "");
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidArgumentException), "invalid_email")]
            public void ShouldThrowInvalidArgumentExceptionOnEmailWrongFormat1()
            {
                IUserService s = new UserService();
                s.RegisterUser("test", "");
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidArgumentException), "invalid_email")]
            public void ShouldThrowInvalidArgumentExceptionOnEmailWrongFormat2()
            {
                IUserService s = new UserService();
                s.RegisterUser("test@test", "");
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidArgumentException), "invalid_email")]
            public void ShouldThrowInvalidArgumentExceptionOnEmailWrongFormat3()
            {
                IUserService s = new UserService();
                s.RegisterUser("test@test.", "");
            }
            #endregion

            #region invalid password
            [TestMethod]
            [ExpectedException(typeof(InvalidArgumentException), "invalid_password")]
            public void ShouldThrowInvalidArgumentExceptionOnPasswordNull()
            {
                IUserService s = new UserService();
                s.RegisterUser("test@test.de", null);
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidArgumentException), "invalid_password")]
            public void ShouldThrowInvalidArgumentExceptionOnPasswordEmpty()
            {
                IUserService s = new UserService();
                s.RegisterUser("test@test.de", "");
            }
            #endregion

            // TODO: further testing requires mocking with microsoft fakes which is not available in professional edition of vs 2013
            //  or using dependency injection
        }
    }
}
