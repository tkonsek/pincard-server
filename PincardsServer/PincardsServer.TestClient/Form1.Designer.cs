﻿namespace PincardsServer.TestClient
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.resultsTextBox = new System.Windows.Forms.TextBox();
            this.uploadWallPictureButton = new System.Windows.Forms.Button();
            this.GetCardsButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.serverUrlTextBox = new System.Windows.Forms.TextBox();
            this.GetMainCardButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.RegisterUserButton = new System.Windows.Forms.Button();
            this.UpdateCardTestButton = new System.Windows.Forms.Button();
            this.CardUploadCardPictureButton = new System.Windows.Forms.Button();
            this.CardUploadWallPictureButton = new System.Windows.Forms.Button();
            this.CardUploadMainPictureButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.CardIDTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(353, 72);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "UpdateProfilePicture";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // resultsTextBox
            // 
            this.resultsTextBox.Location = new System.Drawing.Point(17, 303);
            this.resultsTextBox.Multiline = true;
            this.resultsTextBox.Name = "resultsTextBox";
            this.resultsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.resultsTextBox.Size = new System.Drawing.Size(838, 269);
            this.resultsTextBox.TabIndex = 1;
            // 
            // uploadWallPictureButton
            // 
            this.uploadWallPictureButton.Location = new System.Drawing.Point(353, 101);
            this.uploadWallPictureButton.Name = "uploadWallPictureButton";
            this.uploadWallPictureButton.Size = new System.Drawing.Size(137, 23);
            this.uploadWallPictureButton.TabIndex = 2;
            this.uploadWallPictureButton.Text = "UpdateWallPicture";
            this.uploadWallPictureButton.UseVisualStyleBackColor = true;
            this.uploadWallPictureButton.Click += new System.EventHandler(this.uploadWallPictureButton_Click);
            // 
            // GetCardsButton
            // 
            this.GetCardsButton.Location = new System.Drawing.Point(353, 130);
            this.GetCardsButton.Name = "GetCardsButton";
            this.GetCardsButton.Size = new System.Drawing.Size(137, 23);
            this.GetCardsButton.TabIndex = 3;
            this.GetCardsButton.Text = "GetCards";
            this.GetCardsButton.UseVisualStyleBackColor = true;
            this.GetCardsButton.Click += new System.EventHandler(this.GetCardsButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(397, 46);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(93, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "4";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(353, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "userID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "serverUrl";
            // 
            // serverUrlTextBox
            // 
            this.serverUrlTextBox.Location = new System.Drawing.Point(75, 12);
            this.serverUrlTextBox.Name = "serverUrlTextBox";
            this.serverUrlTextBox.Size = new System.Drawing.Size(775, 20);
            this.serverUrlTextBox.TabIndex = 7;
            this.serverUrlTextBox.Text = "http://pincards.azurewebsites.net/";
            // 
            // GetMainCardButton
            // 
            this.GetMainCardButton.Location = new System.Drawing.Point(353, 159);
            this.GetMainCardButton.Name = "GetMainCardButton";
            this.GetMainCardButton.Size = new System.Drawing.Size(137, 23);
            this.GetMainCardButton.TabIndex = 8;
            this.GetMainCardButton.Text = "GetMainCard";
            this.GetMainCardButton.UseVisualStyleBackColor = true;
            this.GetMainCardButton.Click += new System.EventHandler(this.GetMainCardButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "email";
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(75, 48);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(242, 20);
            this.emailTextBox.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "password";
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(75, 75);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(242, 20);
            this.passwordTextBox.TabIndex = 12;
            // 
            // RegisterUserButton
            // 
            this.RegisterUserButton.Location = new System.Drawing.Point(20, 101);
            this.RegisterUserButton.Name = "RegisterUserButton";
            this.RegisterUserButton.Size = new System.Drawing.Size(297, 23);
            this.RegisterUserButton.TabIndex = 13;
            this.RegisterUserButton.Text = "RegisterUser";
            this.RegisterUserButton.UseVisualStyleBackColor = true;
            this.RegisterUserButton.Click += new System.EventHandler(this.RegisterUserButton_Click);
            // 
            // UpdateCardTestButton
            // 
            this.UpdateCardTestButton.Location = new System.Drawing.Point(714, 78);
            this.UpdateCardTestButton.Name = "UpdateCardTestButton";
            this.UpdateCardTestButton.Size = new System.Drawing.Size(136, 23);
            this.UpdateCardTestButton.TabIndex = 14;
            this.UpdateCardTestButton.Text = "UpdateCardTest";
            this.UpdateCardTestButton.UseVisualStyleBackColor = true;
            this.UpdateCardTestButton.Click += new System.EventHandler(this.UpdateCardTestButton_Click);
            // 
            // CardUploadCardPictureButton
            // 
            this.CardUploadCardPictureButton.Location = new System.Drawing.Point(714, 107);
            this.CardUploadCardPictureButton.Name = "CardUploadCardPictureButton";
            this.CardUploadCardPictureButton.Size = new System.Drawing.Size(136, 23);
            this.CardUploadCardPictureButton.TabIndex = 15;
            this.CardUploadCardPictureButton.Text = "Card.UploadCardPicture";
            this.CardUploadCardPictureButton.UseVisualStyleBackColor = true;
            this.CardUploadCardPictureButton.Click += new System.EventHandler(this.CardUploadCardPictureButton_Click);
            // 
            // CardUploadWallPictureButton
            // 
            this.CardUploadWallPictureButton.Location = new System.Drawing.Point(714, 136);
            this.CardUploadWallPictureButton.Name = "CardUploadWallPictureButton";
            this.CardUploadWallPictureButton.Size = new System.Drawing.Size(136, 23);
            this.CardUploadWallPictureButton.TabIndex = 16;
            this.CardUploadWallPictureButton.Text = "Card.UploadWallPicture";
            this.CardUploadWallPictureButton.UseVisualStyleBackColor = true;
            this.CardUploadWallPictureButton.Click += new System.EventHandler(this.CardUploadWallPictureButton_Click);
            // 
            // CardUploadMainPictureButton
            // 
            this.CardUploadMainPictureButton.Location = new System.Drawing.Point(714, 165);
            this.CardUploadMainPictureButton.Name = "CardUploadMainPictureButton";
            this.CardUploadMainPictureButton.Size = new System.Drawing.Size(136, 23);
            this.CardUploadMainPictureButton.TabIndex = 17;
            this.CardUploadMainPictureButton.Text = "Card.UploadMainPicture";
            this.CardUploadMainPictureButton.UseVisualStyleBackColor = true;
            this.CardUploadMainPictureButton.Click += new System.EventHandler(this.CardUploadMainPictureButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(713, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "cardID";
            // 
            // CardIDTextBox
            // 
            this.CardIDTextBox.Location = new System.Drawing.Point(757, 47);
            this.CardIDTextBox.Name = "CardIDTextBox";
            this.CardIDTextBox.Size = new System.Drawing.Size(93, 20);
            this.CardIDTextBox.TabIndex = 18;
            this.CardIDTextBox.Text = "6";
            this.CardIDTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 584);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CardIDTextBox);
            this.Controls.Add(this.CardUploadMainPictureButton);
            this.Controls.Add(this.CardUploadWallPictureButton);
            this.Controls.Add(this.CardUploadCardPictureButton);
            this.Controls.Add(this.UpdateCardTestButton);
            this.Controls.Add(this.RegisterUserButton);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.GetMainCardButton);
            this.Controls.Add(this.serverUrlTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.GetCardsButton);
            this.Controls.Add(this.uploadWallPictureButton);
            this.Controls.Add(this.resultsTextBox);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox resultsTextBox;
        private System.Windows.Forms.Button uploadWallPictureButton;
        private System.Windows.Forms.Button GetCardsButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox serverUrlTextBox;
        private System.Windows.Forms.Button GetMainCardButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.Button RegisterUserButton;
        private System.Windows.Forms.Button UpdateCardTestButton;
        private System.Windows.Forms.Button CardUploadCardPictureButton;
        private System.Windows.Forms.Button CardUploadWallPictureButton;
        private System.Windows.Forms.Button CardUploadMainPictureButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CardIDTextBox;
    }
}

