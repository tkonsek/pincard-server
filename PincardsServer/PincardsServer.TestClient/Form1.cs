﻿using RemObjects.SDK;
using RemObjects.SDK.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PincardsServer.TestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int userID = getUserID();
            if (userID == -1) return;

            Stream myStream = null;
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "png files (*.png)|*.png|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        // Insert code to read the stream here.
                        using (Binary s = new Binary())
                        {
                            using (myStream)
                            {
                                s.LoadFromStream(myStream);
                            }

                            IpHttpClientChannel channel = new IpHttpClientChannel();
                            channel.TargetUrl = serviceUrl;
                            BinMessage m = new BinMessage();
                            m.UseCompression = false;
                            resultsTextBox.AppendText(CoUserService.Create(m, channel).UpdateProfilePicture(userID, s) + System.Environment.NewLine);

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }

        private void uploadWallPictureButton_Click(object sender, EventArgs e)
        {
            int userID = getUserID();
            if (userID == -1) return;

            Stream myStream = null;
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "png files (*.png)|*.png|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        // Insert code to read the stream here.
                        using (Binary s = new Binary())
                        {
                            using (myStream)
                            {
                                s.LoadFromStream(myStream);
                            }

                            IpHttpClientChannel channel = new IpHttpClientChannel();
                            channel.TargetUrl = serviceUrl; 
                            BinMessage m = new BinMessage();
                            m.UseCompression = false;
                            resultsTextBox.AppendText(CoUserService.Create(m, channel).UpdateWallPicture(userID, s) + System.Environment.NewLine);

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }

        }
        private int getUserID()
        {
            int tester;
            if (!Int32.TryParse(textBox1.Text, out tester))
            {
                MessageBox.Show("userID must be an integer");
                return -1;
            }
            return tester;
        }

        private string serviceUrl
        {
            get
            {
                return serverUrlTextBox.Text + "Service.ashx/binary";
            }
        }

        private void GetCardsButton_Click(object sender, EventArgs e)
        {
            resultsTextBox.AppendText("executing GetCards...");
            int userID = getUserID();
            if (userID == -1) return;

            try
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();

                IpHttpClientChannel channel = new IpHttpClientChannel();
                channel.TargetUrl = serviceUrl;
                pcCard[] cards = CoUserService.Create(new BinMessage(), channel).GetCards(userID);
                resultsTextBox.AppendText("took " + watch.ElapsedMilliseconds + " ms" + System.Environment.NewLine);
                watch.Stop();
                resultsTextBox.AppendText(cards.Length + " cards found:" + System.Environment.NewLine);
                foreach (var card in cards)
                {
                    resultsTextBox.AppendText("cardID: " + card.CardID + System.Environment.NewLine);
                    resultsTextBox.AppendText("   cardInfoKeys: ");
                    foreach (var info in card.CardInfos)
                    {
                        resultsTextBox.AppendText("(" + info.Key + "," + info.Value + "," + info.Type + ")");
                    }
                    resultsTextBox.AppendText(System.Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                resultsTextBox.AppendText("error occured: " + ex.Message + System.Environment.NewLine);
            }
        }

        private void GetMainCardButton_Click(object sender, EventArgs e)
        {
            resultsTextBox.AppendText("executing GetMainCard...");
            int userID = getUserID();
            if (userID == -1) return;

            try
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();

                IpHttpClientChannel channel = new IpHttpClientChannel();
                channel.TargetUrl = serviceUrl;
                pcCard card = CoUserService.Create(new BinMessage(), channel).GetMainCard(userID);
                resultsTextBox.AppendText("took " + watch.ElapsedMilliseconds + " ms" + System.Environment.NewLine);
                watch.Stop();

                resultsTextBox.AppendText("cardID: " + card.CardID + System.Environment.NewLine);
                resultsTextBox.AppendText("   cardInfoKeys: ");
                foreach (var info in card.CardInfos)
                {
                    resultsTextBox.AppendText("(" + info.Key + "," + info.Value + "," + info.Type + ")");
                }
                resultsTextBox.AppendText(System.Environment.NewLine);

            }
            catch (Exception ex)
            {
                resultsTextBox.AppendText("error occured: " + ex.Message + System.Environment.NewLine);
            }

        }

        private void RegisterUserButton_Click(object sender, EventArgs e)
        {
            resultsTextBox.AppendText("executing RegisterUser...");
            
            try
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();

                IpHttpClientChannel channel = new IpHttpClientChannel();
                channel.TargetUrl = serviceUrl;
                pcUserInfo user = CoUserService.Create(new BinMessage(), channel).RegisterUser(emailTextBox.Text, passwordTextBox.Text);
                resultsTextBox.AppendText("took " + watch.ElapsedMilliseconds + " ms" + System.Environment.NewLine);
                watch.Stop();

                resultsTextBox.AppendText("userID: " + user.UserID + System.Environment.NewLine);

            }
            catch (Exception ex)
            {
                resultsTextBox.AppendText("error occured: " + ex.Message + System.Environment.NewLine);
            }

        }

        private void UpdateCardTestButton_Click(object sender, EventArgs e)
        {
            resultsTextBox.AppendText("executing UpdateCard...");

            try
            {
                // get card
                IpHttpClientChannel channel = new IpHttpClientChannel();
                channel.TargetUrl = serviceUrl;
                pcCard c = CoUserService.Create(new BinMessage(), channel).GetMainCard(4);

                // add new card info
                var l = c.CardInfos.ToList<pcCardInfo>();
                l.Add(new pcCardInfo { Key = "key", Value = "value", Type = "type" });
                l.ElementAt(1).Key = "keyChanged";
                l.ElementAt(1).Value = "valueChanged";
                l.ElementAt(1).Type = "typeChanged";
                l.RemoveAt(0);
                
                c.CardInfos = l.ToArray();

                Stopwatch watch = new Stopwatch();
                watch.Start();

                var r = CoCardService.Create(new BinMessage(), channel).UpdateCard(c);
                
                watch.Stop();
                resultsTextBox.AppendText("took " + watch.ElapsedMilliseconds + " ms" + System.Environment.NewLine);

                resultsTextBox.AppendText("result: " + r + System.Environment.NewLine);
            }
            catch (Exception ex)
            {
                resultsTextBox.AppendText("error occured: " + ex.Message + System.Environment.NewLine);
            }
        }


        private int getCardID()
        {
            int tester;
            if (!Int32.TryParse(CardIDTextBox.Text, out tester))
            {
                MessageBox.Show("cardID must be an integer");
                return -1;
            }
            return tester;
        }
        private void updateCardPicture(CardPictureType type)
        {
            int cardID = getCardID();
            if (cardID == -1) return;

            Stream myStream = null;
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "png files (*.png)|*.png|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        // Insert code to read the stream here.
                        using (Binary s = new Binary())
                        {
                            using (myStream)
                            {
                                s.LoadFromStream(myStream);
                            }

                            IpHttpClientChannel channel = new IpHttpClientChannel();
                            channel.TargetUrl = serviceUrl;
                            BinMessage m = new BinMessage();
                            m.UseCompression = false;
                            resultsTextBox.AppendText(
                                CoCardService.Create(m, channel).UpdatePicture(cardID, type, s) + System.Environment.NewLine);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }
            }
        }
        private void CardUploadCardPictureButton_Click(object sender, EventArgs e)
        {
            updateCardPicture(CardPictureType.Card);
        }

        private void CardUploadWallPictureButton_Click(object sender, EventArgs e)
        {
            updateCardPicture(CardPictureType.Wall);
        }

        private void CardUploadMainPictureButton_Click(object sender, EventArgs e)
        {
            updateCardPicture(CardPictureType.Main);
        }
    }
}
