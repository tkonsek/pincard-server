﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PincardsServer.Persistency;
using System.IO;
using System.Drawing;

namespace PincardsServer.Controllers
{
    public class CardController : Controller
    {
        private PincardsEntities db = new PincardsEntities();

        // GET: /Card/
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        // GET: /Card/Create
        public ActionResult Create(long? userId)
        {
            if (userId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.User.Find(userId);
            if (user == null)
            {
                return HttpNotFound();
            }

            ViewBag.idUserID = new SelectList(db.User, "idUserID", "sMail", user.idUserID);
            return View();
        }

        // POST: /Card/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idCardID, idUserID")] Card card, HttpPostedFileBase wallPicture, HttpPostedFileBase mainPicture, HttpPostedFileBase cardPicture)
        {
            if (ModelState.IsValid)
            {
                db.Card.Add(card);
                db.SaveChanges();
                StoreCardPictures(card, wallPicture, mainPicture, cardPicture);
                return RedirectToAction("Index", "Home", new { cards = card.idCardID });
            }

            ViewBag.idUserID = new SelectList(db.User, "idUserID", "sMail", card.idUserID);
            return View(card);
        }

        // GET: /Card/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Card card = db.Card.Find(id);
            if (card == null)
            {
                return HttpNotFound();
            }
            ViewBag.idUserID = new SelectList(db.User, "idUserID", "sMail", card.idUserID);
            return View(card);
        }

        // POST: /Card/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(long idCardID, long? idUserID, HttpPostedFileBase wallPicture, HttpPostedFileBase mainPicture, HttpPostedFileBase cardPicture)
        {
            Card card = db.Card.Find(idCardID);
            if (card == null)
            {
                return HttpNotFound();
            }

            card.idUserID = idUserID;
            StoreCardPictures(card, wallPicture, mainPicture, cardPicture);
            db.Entry(card).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", "Home", new { cards = card.idCardID });
        }

        // GET: /Card/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Card card = db.Card.Find(id);
            if (card == null)
            {
                return HttpNotFound();
            }
            return View(card);
        }

        // POST: /Card/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Card card = db.Card.Find(id);
            long? userId = card.idUserID;

            db.CardInfo.RemoveRange(card.CardInfo);
            db.Card.Remove(card);
            db.SaveChanges();
            Storage.DeleteCardPictures(id);
            return RedirectToAction("Index", "Home", new { users = userId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void StoreCardPictures(Card card, HttpPostedFileBase wallPicture, HttpPostedFileBase mainPicture, HttpPostedFileBase cardPicture)
        {
            if (cardPicture != null && cardPicture.ContentLength > 0 && cardPicture.ContentType == "image/png")
            {
                // Höhe und Breite abspeichern.
                using (Image image = Image.FromStream(cardPicture.InputStream))
                {
                    card.iCardPictureHeight = (short)image.Height;
                    card.iCardPictureWidth = (short)image.Width;
                }

                cardPicture.InputStream.Position = 0;

                // Bild abspeichern.
                using (MemoryStream stream = new MemoryStream())
                {
                    cardPicture.InputStream.CopyTo(stream);
                    stream.Flush();
                    stream.Position = 0;
                    card.sCardPictureUrl = Storage.UploadCardCardPicture(card.idCardID, stream);
                }

                db.SaveChanges();
            }

            if (mainPicture != null && mainPicture.ContentLength > 0 && mainPicture.ContentType == "image/png")
            {
                // Höhe und Breite abspeichern.
                using (Image image = Image.FromStream(mainPicture.InputStream))
                {
                    card.iMainPictureHeight = (short)image.Height;
                    card.iMainPictureWidth = (short)image.Width;
                }

                mainPicture.InputStream.Position = 0;

                // Bild abspeichern.
                using (MemoryStream stream = new MemoryStream())
                {
                    mainPicture.InputStream.CopyTo(stream);
                    stream.Flush();
                    stream.Position = 0;
                    card.sMainPictureUrl = Storage.UploadCardMainPicture(card.idCardID, stream);
                }

                db.SaveChanges();
            }

            if (wallPicture != null && wallPicture.ContentLength > 0 && wallPicture.ContentType == "image/png")
            {
                // Höhe und Breite abspeichern.
                using (Image image = Image.FromStream(wallPicture.InputStream, true, true))
                {
                    card.iWallPictureHeight = (short)image.Height;
                    card.iWallPictureWidth = (short)image.Width;
                }

                wallPicture.InputStream.Position = 0;

                // Bild abspeichern.
                using (MemoryStream stream = new MemoryStream())
                {
                    wallPicture.InputStream.CopyTo(stream);
                    stream.Flush();
                    stream.Position = 0;
                    card.sWallPictureUrl = Storage.UploadCardWallPicture(card.idCardID, stream);
                }

                db.SaveChanges();
            }
        }
    }
}
