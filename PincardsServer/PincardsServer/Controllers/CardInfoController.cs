﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PincardsServer.Persistency;

namespace PincardsServer.Controllers
{
    public class CardInfoController : Controller
    {
        private PincardsEntities db = new PincardsEntities();

        // GET: /CardInfo/Create
        public ActionResult Create(long? cardId)
        {
            if (cardId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Card card = db.Card.Find(cardId);
            if (card == null)
            {
                return HttpNotFound();
            }

            CardInfo cardInfo = new CardInfo()
            {
                Key = "-",
                Value = "-",
                Type = "-",
                iPosition = db.CardInfo.Where(i => i.idCardID == cardId).Count(),
                idCardID = cardId
            };

            db.CardInfo.Add(cardInfo);
            db.SaveChanges();
            return Redirect(Request.UrlReferrer.ToString());
        }

        // POST: /CardInfo/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(long idCardInfoID, string key, string value, string type)
        {
            CardInfo info = db.CardInfo.Find(idCardInfoID);
            if (info == null)
            {
                return HttpNotFound();
            }

            info.Key = key;
            info.Value = value;
            info.Type = type;
            db.Entry(info).State = EntityState.Modified;
            db.SaveChanges();
            return Redirect(Request.UrlReferrer.ToString());
        }

        // GET: /CardInfo/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CardInfo cardinfo = db.CardInfo.Find(id);
            if (cardinfo == null)
            {
                return HttpNotFound();
            }
            cardinfo.iPosition = -1;
            this.Rearrange(cardinfo);
            db.CardInfo.Remove(cardinfo);
            db.SaveChanges();
            return Redirect(Request.UrlReferrer.ToString());
        }

        private void Rearrange(CardInfo movedInfo)
        {
            long? cardID = movedInfo.idCardID;

            // Alle Infos einer Card außer der verschobenen Info
            IQueryable<CardInfo> infos = db.CardInfo.Where(i => i.idCardID == cardID &&
                i.idCardInfoID != movedInfo.idCardInfoID).OrderBy(i => i.iPosition);

            IEnumerator<CardInfo> it = infos.GetEnumerator();

            int allInfosCount = infos.Count();

            if (movedInfo.iPosition >= 0)
            {
                allInfosCount++;
            }

            for (int i = 0; i < allInfosCount; i++)
            {
                if (i != movedInfo.iPosition)
                {
                    it.MoveNext();
                    it.Current.iPosition = i;
                }
            }
        }

        public ActionResult Move(int? id, string direction)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CardInfo cardinfo = db.CardInfo.Find(id);
            if (cardinfo == null)
            {
                return HttpNotFound();
            }

            if (direction.ToLower().Equals("up"))
            {
                if (cardinfo.iPosition <= 0)
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                else if (cardinfo.iPosition == null)
                {
                    cardinfo.iPosition = 0;
                }
                else
                {
                    cardinfo.iPosition--;
                }

                this.Rearrange(cardinfo);
                db.SaveChanges();
            }
            else if (direction.ToLower().Equals("down"))
            {
                int cardInfosCount = cardinfo.Card.CardInfo.Count();

                if (cardinfo.iPosition >= cardInfosCount - 1)
                {
                    return Redirect(Request.UrlReferrer.ToString());
                }
                else if (cardinfo.iPosition == null)
                {
                    cardinfo.iPosition = cardInfosCount - 1;
                }
                else
                {
                    cardinfo.iPosition++;
                }

                this.Rearrange(cardinfo);
                db.SaveChanges();
            }

            return Redirect(Request.UrlReferrer.ToString());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
