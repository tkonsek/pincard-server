﻿using PincardsServer.Persistency;
using PincardsServer.Services;
using RemObjects.SDK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PincardsServer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ICollection<CardInfo> cardInfo = null;
            User selectedUser = null;
            Card selectedCard = null;
            SelectList cards = null;
            SelectList users = null;

            long userId;
            long.TryParse(Request.QueryString.Get("users"), out userId);

            long cardId;
            long.TryParse(Request.QueryString.Get("cards"), out cardId);

            string searchtype = Request.Form["searchtype"];
            string searchtext = Request.Form["searchtext"];

            using (PincardsEntities db = new PincardsEntities())
            {
                if (!string.IsNullOrWhiteSpace(searchtext))
                {
                    searchtext = searchtext.ToLower().Trim();

                    if (searchtype.Equals("User"))
                    {
                        IQueryable<User> foundUsers = db.User.Where(
                            u => (u.sFirstName != null && u.sFirstName.ToLower().Contains(searchtext)) ||
                                (u.sLastName != null && u.sLastName.ToLower().Contains(searchtext)) ||
                                (u.sMail != null && u.sMail.ToLower().Contains(searchtext)));

                        ViewBag.UserSearchResult = foundUsers.ToList();
                    }
                    else if (searchtype.Equals("Card"))
                    {
                        IQueryable<CardInfo> foundCardInfos = db.CardInfo.Where(
                            i => i.idCardID != null &&
                                (i.Key != null && i.Key.ToLower().Contains(searchtext) ||
                                i.Value != null && i.Value.ToLower().Contains(searchtext) ||
                                i.Type != null && i.Type.ToLower().Contains(searchtext)));

                        ViewBag.CardInfoSearchResult = foundCardInfos.ToList();
                    }
                }

                selectedUser = db.User.Find(userId);
                selectedCard = db.Card.Find(cardId);

                if (selectedUser == null && selectedCard == null)
                {
                    selectedCard = db.Card.FirstOrDefault();
                }

                if (selectedUser == null && selectedCard != null)
                {
                    selectedUser = selectedCard.User;
                }
                else if (selectedUser != null && selectedCard == null)
                {
                    selectedCard = selectedUser.Card.FirstOrDefault();
                }

                if (selectedUser != null)
                {
                    long? selectedCardId = null;
                    if (selectedCard != null)
                    {
                        selectedCardId = selectedCard.idCardID;
                    }
                    cards = new SelectList(selectedUser.Card.OrderBy(c => c.idCardID), "idCardID", "idCardID", selectedCardId);
                    users = new SelectList(db.User.OrderBy(u => u.idUserID).ToList(), "idUserID", "idUserID", selectedUser.idUserID);
                }
                else
                {
                    users = new SelectList(db.User.OrderBy(u => u.idUserID).ToList(), "idUserID", "idUserID");
                }

                if (selectedCard != null)
                {
                    cardInfo = db.CardInfo.Where(i => i.idCardID == selectedCard.idCardID).OrderBy(i => i.iPosition).ToList();
                }

                ViewData["cards"] = cards;
                ViewData["cardInfo"] = cardInfo;
                ViewData["selectedUser"] = selectedUser;
                ViewData["selectedCard"] = selectedCard;
                ViewData["users"] = users;

                return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Test()
        {
            try
            {
                IpHttpClientChannel channel = new IpHttpClientChannel();
                string serverUrl = HttpContext.Request.Url.AbsoluteUri.Substring(0, HttpContext.Request.Url.AbsoluteUri.Length - 9);
                channel.TargetUrl = serverUrl + "Service.ashx/binary";
                ViewBag.Result = CoMainService.Create(new BinMessage(), channel).GetPinById(1);
                //ViewBag.Result = "test result";

                ViewBag.Result += "registerUser: ";
                ViewBag.Result += " - " + CoUserService.Create(new BinMessage(), channel).RegisterUser("test1@test.de", "test");

                ViewBag.Result += "getUserInfo: ";
                var user = CoUserService.Create(new BinMessage(), channel).GetUserInfo(1);
                ViewBag.Result += " - " + user.Email;

                ViewBag.Result += "updateUserInfo: ";
                ViewBag.Result += " - " + CoUserService.Create(new BinMessage(), channel).UpdateUserInfo(
                    new pcUserInfo
                    {
                        UserID = 4,
                        FirstName = "Barney",
                        LastName = "Stinson",
                        Gender = "m",
                        Birthday = DateTime.UtcNow,
                        About = "working at GNB"
                    });

                //ViewBag.Result += "updateProfilePicture: ";
                //ViewBag.Result += " - " + CoUserService.Create(new BinMessage(), channel).UpdateProfilePicture(0, null);

                //ViewBag.Result += "updateWallPicture: ";
                //ViewBag.Result += " - " + CoUserService.Create(new BinMessage(), channel).UpdateWallPicture(0, null);
            }
            catch (Exception e)
            {
                ViewBag.Result = e.Message;
            }
            return View();
        }
    }
}