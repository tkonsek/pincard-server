﻿using PincardsServer.Services;
using RemObjects.SDK;
using RemObjects.SDK.Server.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PincardsServer
{

    public class HandlerFactory : WebProcessor
    {
        private static HandlerFactory fInstance = new HandlerFactory();
        public static HandlerFactory Instance { get { return fInstance; } }

        //private MemorySessionManager MemorySessionManager;

        private HandlerFactory()
        {

            //MemorySessionManager = new RemObjects.SDK.Server.MemorySessionManager();

            TypeManager.AddType(typeof(MainService));

            // Entry assembly is set to the assembly containing the RODL resource.
            //EntryAssembly = typeof(ASP.NETClassLib.NewService).Assembly;
            // This security feature defines if the RODL is publicly visible or not.
            //ServeRodl = true;
            ServeRodl = false;
            // Add the BinMessage message type to this channel. This will be accessible via http://path/to/Handler.ahsx/binary
            Dispatchers.Add("binary", new BinMessage());
            // Optionally, soap can be added with:
            // Dispatchers.Add("soap", new SoapMessage());

            // DOESNT WORK IN AZURE
            // to allow javascript calls from other websites
            //this.SendCrossOriginHeader = true;
        }
    }


    /// <summary>
    /// Zusammenfassungsbeschreibung für Services
    /// </summary>
    public class Service : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "Text/unformatiert";
            //context.Response.Write("Hello World");
            HandlerFactory.Instance.ProcessRequest(context);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}