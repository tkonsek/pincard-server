//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.34011
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PincardsServer.Services
{
    using System;
    using RemObjects.SDK;
    using RemObjects.SDK.Types;
    using RemObjects.SDK.Server;
    using RemObjects.SDK.Server.ClassFactories;
    using PincardsServer.Persistency;
    using System.Linq;

    [RemObjects.SDK.Server.ClassFactories.StandardClassFactory()]
    [RemObjects.SDK.Server.Service(Name = "CardService", InvokerClass = typeof(CardService_Invoker), ActivatorClass = typeof(CardService_Activator))]
    public class CardService : RemObjects.SDK.Server.Service, ICardService
    {
        private System.ComponentModel.Container components = null;
        public CardService() :
            base()
        {
            this.InitializeComponent();
        }
        private void InitializeComponent()
        {
        }
        protected override void Dispose(bool aDisposing)
        {
            if (aDisposing)
            {
                if ((this.components != null))
                {
                    this.components.Dispose();
                }
            }
            base.Dispose(aDisposing);
        }
        public virtual pcCard UpdateCard(pcCard card)
        {
            // assert that CardID is set
            if (card.CardID == 0)
            {
                throw new InvalidArgumentException("CardID_not_set");
            }

            using (PincardsEntities db = new PincardsEntities())
            {
                // check that card exists
                var dbCard = db.Card.Where(c => c.idCardID == card.CardID).SingleOrDefault();
                if (dbCard == null)
                {
                    throw new InvalidArgumentException("card_does_not_exist");
                }

                //string result = "";
                //result += "new card info objects: ";
                // new card info objects
                var newCardInfos = card.CardInfos.Where(c => c.CardInfoID == 0);
                foreach (pcCardInfo info in newCardInfos)
                {
                    //result += "(" + info.Key + "," + info.Value + "), ";

                    db.CardInfo.Add(new CardInfo
                    {
                        Card = dbCard,
                        Key = info.Key,
                        Type = info.Type,
                        Value = info.Value
                    });
                }


                // cards that should be deleted
                var cardsToBeDeleted = dbCard.CardInfo.Where(c => !card.CardInfos.Select(a => a.CardInfoID).Contains(c.idCardInfoID)).ToList();
                foreach (CardInfo info in cardsToBeDeleted)
                {
                    db.CardInfo.Remove(info);
                }

                // cards that should be updated
                var cardsToBeUpdated = dbCard.CardInfo.Where(c => card.CardInfos.Select(a => a.CardInfoID).Contains(c.idCardInfoID)).ToList();
                foreach (CardInfo info in cardsToBeUpdated)
                {
                    var newValues = card.CardInfos.Where(c => c.CardInfoID == info.idCardInfoID).SingleOrDefault();
                    // should actually always be not null
                    if(newValues != null){
                        info.Key = newValues.Key;
                        info.Value = newValues.Value;
                        info.Type = newValues.Type;
                    }                    
                }

                db.SaveChanges();

                // transform to DTO and return
                return new pcCard
                {
                    CardID = dbCard.idCardID,
                    UserID = (long)dbCard.idUserID,
                    WallPictureUrl = dbCard.sWallPictureUrl,
                    MainPictureUrl = dbCard.sMainPictureUrl,
                    CardPictureUrl = dbCard.sCardPictureUrl,
                    CardInfos = dbCard.CardInfo.Select(ci => new pcCardInfo
                    {
                        CardInfoID = ci.idCardInfoID,
                        Key = ci.Key,
                        Value = ci.Value,
                        Type = ci.Type
                    }).ToArray()
                };
            }
        }

        public string UpdatePicture(long cardID, CardPictureType pictureType, Binary picture)
        {
            // assert that cardID is set
            if (cardID == 0)
            {
                throw new InvalidArgumentException("cardID_not_set");
            }

            using (PincardsEntities db = new PincardsEntities())
            {
                // check that card exists
                var card = db.Card.Where(c => c.idCardID == cardID).SingleOrDefault();
                if (card == null)
                {
                    throw new InvalidArgumentException("card_does_not_exist");
                }

                // upload picture
                string uri = String.Empty;
                switch (pictureType)
                {
                    case CardPictureType.Wall:
                        uri = Storage.UploadCardWallPicture(cardID, picture);
                        card.sWallPictureUrl = uri;
                        break;
                    case CardPictureType.Card:
                        uri = Storage.UploadCardCardPicture(cardID, picture);
                        card.sCardPictureUrl = uri;
                        break;
                    case CardPictureType.Main:
                        uri = Storage.UploadCardMainPicture(cardID, picture);
                        card.sMainPictureUrl = uri;
                        break;
                }

                // update card in db
                db.SaveChanges();

                return uri;
            }
        }
    }
}
