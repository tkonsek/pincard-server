//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.34011
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PincardsServer.Services
{
    using System;
    using RemObjects.SDK;
    using RemObjects.SDK.Types;
    using RemObjects.SDK.Server;
    using RemObjects.SDK.Server.ClassFactories;
    using System.Security.Cryptography;
    using System.Text;
    using System.Linq;
    using PincardsServer.Persistency;
    using PincardsServer.Common;
    using System.Threading.Tasks;

    [RemObjects.SDK.Server.ClassFactories.StandardClassFactory()]
    [RemObjects.SDK.Server.Service(Name = "UserService", InvokerClass = typeof(UserService_Invoker), ActivatorClass = typeof(UserService_Activator))]
    public class UserService : RemObjects.SDK.Server.Service, IUserService
    {
        private System.ComponentModel.Container components = null;
        public UserService() :
            base()
        {
            this.InitializeComponent();
        }
        private void InitializeComponent()
        {
        }
        protected override void Dispose(bool aDisposing)
        {
            if (aDisposing)
            {
                if ((this.components != null))
                {
                    this.components.Dispose();
                }
            }
            base.Dispose(aDisposing);
        }
        public virtual pcUserInfo GetUserInfo(long userID)
        {
            // validate userID
            if (userID == 0)
            {
                throw new InvalidArgumentException("userID_invalid");
            }

            using (PincardsEntities db = new PincardsEntities())
            {
                // check that user exists
                User user = db.User.Where(u => u.idUserID == userID).SingleOrDefault();
                if (user == null)
                {
                    throw new InvalidArgumentException("user_does_not_exist");
                }

                // map db object to DTO
                return new pcUserInfo
                {
                    UserID = userID,
                    Email = user.sMail,
                    FirstName = user.sFirstName,
                    LastName = user.sLastName,
                    Gender = user.sGender,
                    ProfilePictureUrl = user.sProfilePictureUrl,
                    WallPictureUrl = user.sWallPictureUrl,
                    Birthday = user.dBirthday == null ? DateTime.MinValue : (DateTime)user.dBirthday,
                    About = user.sAbout
                };
            }
        }

        /// <summary>
        /// creates a new user with email and password
        /// throws InvalidArgumentException if email is already in use
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public virtual pcUserInfo RegisterUser(string email, string password)
        {
            // check parameters
            if (string.IsNullOrWhiteSpace(email) || !Validation.IsValidEmailAddress(email))
            {
                throw new InvalidArgumentException("invalid_email");
            }
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new InvalidArgumentException("invalid_password");
            }

            using (PincardsEntities db = new PincardsEntities())
            {
                // look for an existing user with this email
                var existing = db.User.Where(u => u.sMail == email).SingleOrDefault();
                if (existing != null)
                {
                    throw new InvalidArgumentException("email_already_in_use");
                }

                // generate salt and hash the password
                HashAlgorithm alg = new SHA256Managed();
                string salt = string.Concat(Guid.NewGuid().ToByteArray().Select(g => g.ToString("x2")));
                string hashedPassword = string.Concat(alg.ComputeHash(Encoding.UTF8.GetBytes(password + salt)).Select(h => h.ToString("x2")));

                // create the new user in the database
                User user = db.User.Add(new User
                    {
                        sMail = email,
                        sPassword = hashedPassword,
                        sSalt = salt,
                    });
                // create user's main card
                Card card = db.Card.Add(new Card
                {
                    User = user
                });
                db.SaveChanges();


                // return user info object
                return new pcUserInfo
                {
                    Email = user.sMail,
                    UserID = user.idUserID
                };
            }
        }

        /// <summary>
        /// updates the user in database
        /// the following attributes are updated in db
        /// - sFirstName
        /// - sLastName
        /// - sGender
        /// - dBirthday
        /// - sAbout
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public virtual bool UpdateUserInfo(pcUserInfo user)
        {
            // assert that UserID is set
            if (user.UserID == 0)
            {
                throw new InvalidArgumentException("userID_not_set");
            }
            // check gender length
            if (user.Gender.Length > 1)
            {
                throw new InvalidArgumentException("gender_has_more_than_one_character");
            }

            using (PincardsEntities db = new PincardsEntities())
            {
                // check that user exists
                var dbUser = db.User.Where(u => u.idUserID == user.UserID).SingleOrDefault();
                if (dbUser == null)
                {
                    throw new InvalidArgumentException("user_does_not_exist");
                }

                //sMail
                //sPassword
                //sSalt
                dbUser.sFirstName = user.FirstName;
                dbUser.sLastName = user.LastName;
                dbUser.sGender = user.Gender;
                //dbUser.sProfilePictureUrl
                //dbUser.sWallPictureUrl
                dbUser.dBirthday = user.Birthday;
                dbUser.sAbout = user.About;

                db.SaveChanges();
            }
            return true;
        }
        public virtual string UpdateProfilePicture(long userID, RemObjects.SDK.Types.Binary picture)
        {
            // assert that UserID is set
            if (userID == 0)
            {
                throw new InvalidArgumentException("userID_not_set");
            }

            using (PincardsEntities db = new PincardsEntities())
            {
                // check that user exists
                var user = db.User.Where(u => u.idUserID == userID).SingleOrDefault();
                if (user == null)
                {
                    throw new InvalidArgumentException("user_does_not_exist");
                }

                string uri = Storage.UploadProfilePicture(userID, picture);

                user.sProfilePictureUrl = uri;
                db.SaveChanges();

                return uri;
            }
        }
        public virtual string UpdateWallPicture(long userID, RemObjects.SDK.Types.Binary picture)
        {

            // assert that UserID is set
            if (userID == 0)
            {
                throw new InvalidArgumentException("userID_not_set");
            }

            using (PincardsEntities db = new PincardsEntities())
            {
                // check that user exists
                var user = db.User.Where(u => u.idUserID == userID).SingleOrDefault();
                if (user == null)
                {
                    throw new InvalidArgumentException("user_does_not_exist");
                }

                string uri = Storage.UploadWallPicture(userID, picture);

                user.sWallPictureUrl = uri;
                db.SaveChanges();

                return uri;
            }
        }


        public pcCard[] GetCards(long userID)
        {
            if (userID == 0)
            {
                throw new InvalidArgumentException("userID_not_set");
            }

            using (PincardsEntities db = new PincardsEntities())
            {
                // get cards from db
                var dbCards = db.Card
                    .Where(c => c.idUserID == userID)
                    .ToArray();

                // transform to DTO
                return dbCards.Select(c => new pcCard
                {
                    CardID = c.idCardID,
                    UserID = (long)c.idUserID,
                    WallPictureUrl = c.sWallPictureUrl,
                    MainPictureUrl = c.sMainPictureUrl,
                    CardPictureUrl = c.sCardPictureUrl,
                    CardInfos = c.CardInfo.Select(ci => new pcCardInfo
                    {
                        Key = ci.Key,
                        Value = ci.Value,
                        Type = ci.Type
                    }).ToArray()
                }).ToArray();

            }
        }

        // TODO: performance maybe union all db calls into a stored procedure
        public pcCard GetMainCard(long userID)
        {
            if (userID == 0)
            {
                throw new InvalidArgumentException("userID_not_set");
            }

            using (PincardsEntities db = new PincardsEntities())
            {
                if (!db.User.Any(u => u.idUserID == userID))
                {
                    throw new InvalidArgumentException("user_does_not_exist");
                }

                // get first card for this user
                var dbCard = db.Card.Where(c => c.idUserID == userID).FirstOrDefault();

                // create card if user doesnt have any
                if (dbCard == null)
                {
                    dbCard = new Card { idUserID = userID };
                    db.Card.Add(dbCard);
                    db.SaveChanges();
                }

                // transform to DTO and return
                return new pcCard
                {
                    CardID = dbCard.idCardID,
                    UserID = (long)dbCard.idUserID,
                    WallPictureUrl = dbCard.sWallPictureUrl,
                    MainPictureUrl = dbCard.sMainPictureUrl,
                    CardPictureUrl = dbCard.sCardPictureUrl,
                    CardInfos = dbCard.CardInfo.Select(ci => new pcCardInfo
                    {
                        CardInfoID = ci.idCardInfoID,
                        Key = ci.Key,
                        Value = ci.Value,
                        Type = ci.Type
                    }).ToArray()
                };
            }
        }
    }
}
