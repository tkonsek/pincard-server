﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PincardsServer.Persistency
{
    public static class Storage
    {
        public static string UploadProfilePicture(long userID, MemoryStream stream)
        {
            if (userID == 0)
            {
                throw new ArgumentOutOfRangeException("userID must not be 0");
            }
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            
            // build file name
            string fileName = userID + ".png";

            // upload file
            return UploadFile(stream, "profile-pictures", fileName);
        }

        public static string UploadWallPicture(long userID, MemoryStream stream)
        {
            if (userID == 0)
            {
                throw new ArgumentOutOfRangeException("userID must not be 0");
            }
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            // build file name
            string fileName = userID + ".png";

            // upload file
            return UploadFile(stream, "wall-pictures", fileName);
        }

        private static string UploadCardPicture(string type, long cardId, MemoryStream stream)
        {
            if (cardId == 0)
            {
                throw new ArgumentOutOfRangeException("cardID must not be 0");
            }
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            // build file name
            string fileName = String.Format("{0}-{1}.png", cardId, type);

            // upload file
            return UploadFile(stream, "card-pictures", fileName);
        }

        public static string UploadCardCardPicture(long cardId, MemoryStream stream)
        {
            return UploadCardPicture("card", cardId, stream);
        }

        public static string UploadCardWallPicture(long cardId, MemoryStream stream)
        {
            return UploadCardPicture("wall", cardId, stream);
        }

        public static string UploadCardMainPicture(long cardId, MemoryStream stream)
        {
            return UploadCardPicture("main", cardId, stream);
        }

        private static string UploadFile(Stream stream, string containerName, string fileName)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            
            // Retrieve reference to the blob
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

            //// Create or overwrite the "myblob" blob with contents from a local file.
            //using (var fileStream = System.IO.File.OpenRead(@"path\myfile"))
            //{
            //    blockBlob.UploadFromStream(fileStream);
            //}
            blockBlob.UploadFromStream(stream);

            return blockBlob.Uri.AbsoluteUri;
        }

        private static bool DeleteFile(string containerName, string fileName)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            // Retrieve reference to the blob
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);

            return blockBlob.DeleteIfExists();
        }

        public static void DeleteCardPictures(long cardId)
        {
            if (cardId == 0)
            {
                throw new ArgumentOutOfRangeException("cardID must not be 0");
            }

            string container = "card-pictures";

            string fileName = cardId + "-card.png";
            DeleteFile(container, fileName);

            fileName = cardId + "-wall.png";
            DeleteFile(container, fileName);

            fileName = cardId + "-main.png";
            DeleteFile(container, fileName);
        }
    }
}
