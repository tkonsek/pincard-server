//------------------------------------------------------------------------------
// <auto-generated>
//     Der Code wurde von einer Vorlage generiert.
//
//     Manuelle Änderungen an dieser Datei führen möglicherweise zu unerwartetem Verhalten der Anwendung.
//     Manuelle Änderungen an dieser Datei werden überschrieben, wenn der Code neu generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PincardsServer.Persistency
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pin
    {
        public Pin()
        {
            this.PinCardLink = new HashSet<PinCardLink>();
        }
    
        public long idPinID { get; set; }
        public string sName { get; set; }
        public string sDescription { get; set; }
        public Nullable<int> idPinTypeID { get; set; }
        public Nullable<long> idLocationID { get; set; }
    
        public virtual Location Location { get; set; }
        public virtual PinType PinType { get; set; }
        public virtual ICollection<PinCardLink> PinCardLink { get; set; }
    }
}
