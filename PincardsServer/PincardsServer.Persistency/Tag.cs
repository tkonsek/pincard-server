//------------------------------------------------------------------------------
// <auto-generated>
//     Der Code wurde von einer Vorlage generiert.
//
//     Manuelle Änderungen an dieser Datei führen möglicherweise zu unerwartetem Verhalten der Anwendung.
//     Manuelle Änderungen an dieser Datei werden überschrieben, wenn der Code neu generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PincardsServer.Persistency
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tag
    {
        public Tag()
        {
            this.CardTagLink = new HashSet<CardTagLink>();
            this.TagViewLink = new HashSet<TagViewLink>();
        }
    
        public int idTagID { get; set; }
        public string sValue { get; set; }
        public string Column2 { get; set; }
    
        public virtual ICollection<CardTagLink> CardTagLink { get; set; }
        public virtual ICollection<TagViewLink> TagViewLink { get; set; }
    }
}
