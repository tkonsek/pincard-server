﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Der Code wurde von einer Vorlage generiert.
//
//     Manuelle Änderungen an dieser Datei führen möglicherweise zu unerwartetem Verhalten der Anwendung.
//     Manuelle Änderungen an dieser Datei werden überschrieben, wenn der Code neu generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PincardsServer.Persistency
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class PincardsEntities : DbContext
    {
        public PincardsEntities()
            : base("name=PincardsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Card> Card { get; set; }
        public virtual DbSet<CardInfo> CardInfo { get; set; }
        public virtual DbSet<CardProfileLink> CardProfileLink { get; set; }
        public virtual DbSet<CardTagLink> CardTagLink { get; set; }
        public virtual DbSet<CardViewLink> CardViewLink { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<Pin> Pin { get; set; }
        public virtual DbSet<PinCardLink> PinCardLink { get; set; }
        public virtual DbSet<PinType> PinType { get; set; }
        public virtual DbSet<Profile> Profile { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<TagViewLink> TagViewLink { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<View> View { get; set; }
    }
}
